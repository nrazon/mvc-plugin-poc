﻿using System.ComponentModel.Composition;
using System.Web.Mvc;
using System.Web.Routing;
using PluginPoc.Core.Modules;
using PluginPoc.Core.Plugin;
using PluginPoc.Plugin1.Services;

namespace PluginPoc.Plugin1
{
  [Export(typeof(IPluginConfiguration))]
  public class PluginConfiguration : AbstractPluginConfiguration
  {
    public override void RegisterRoutes(RouteCollection routes)
    {
      routes.MapRoute(
          name: "Plugin1",
          url: "CustomRoute/{action}/{id}",
          defaults: new { controller = "Plugin1", action = "Index", id = UrlParameter.Optional }
      );
    }

    public override void RegisterServices(Ninject.IKernel kernel)
    {
      kernel.Bind<IBarService>()
        .To<BarService>()
        .InHybridScope();
    }
  }
}
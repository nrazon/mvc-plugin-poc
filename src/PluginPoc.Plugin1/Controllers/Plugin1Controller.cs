﻿using System.Web.Mvc;
using PluginPoc.Core.Services;
using PluginPoc.Plugin1.Services;

namespace PluginPoc.Plugin1.Controllers
{
  public class Plugin1Controller : Controller
  {
    private readonly IFooService fooService;
    private readonly IBarService barService;

    public Plugin1Controller(IFooService fooService, IBarService barService)
    {
      this.fooService = fooService;
      this.barService = barService;
    }

    public ActionResult Index()
    {
      return View();
    }
  }
}

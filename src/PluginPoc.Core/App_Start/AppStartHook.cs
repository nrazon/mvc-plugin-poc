using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.WebPages;
using PluginPoc.Core.App_Start;
using PluginPoc.Core.Modules;
using PluginPoc.Core.Plugin;

[assembly: WebActivator.PreApplicationStartMethod(typeof(AppStartHook), "PreApplicationStart")]
[assembly: WebActivator.PostApplicationStartMethod(typeof(AppStartHook), "PostApplicationStart")]
[assembly: WebActivator.ApplicationShutdownMethodAttribute(typeof(AppStartHook), "ApplicationShutdown")]
namespace PluginPoc.Core.App_Start
{
  public static class AppStartHook
  {
    public static void PreApplicationStart()
    {
      NinjectWebCommon.Start();
      PluginLoader.Load();
    }

    public static void PostApplicationStart()
    {
      var assemblies = PluginLoader.Plugins.Select(x => x.Assembly).ToArray();

      var engine = new PluginViewEngine(assemblies)
      {
        UsePhysicalViewsIfNewer = HttpContext.Current.Request.IsLocal
      };

      ViewEngines.Engines.Insert(0, engine);
      VirtualPathFactoryManager.RegisterVirtualPathFactory(engine);
    }

    public static  void ApplicationShutdown()
    {
      NinjectWebCommon.Stop();
    }
  }
}

﻿using System.Reflection;
using System.Web.Routing;
using Ninject;

namespace PluginPoc.Core.Plugin
{
  public abstract class AbstractPluginConfiguration : IPluginConfiguration
  {
    public virtual void RegisterRoutes(RouteCollection routes)
    {
    }

    public virtual void RegisterServices(IKernel kernel)
    {
    }

    public Assembly Assembly
    {
      get { return GetType().Assembly; }
    }
  }
}
﻿using System.Reflection;
using System.Web.Routing;
using Ninject;

namespace PluginPoc.Core.Plugin
{
  public interface IPluginConfiguration
  {
    void RegisterRoutes(RouteCollection routes);
    void RegisterServices(IKernel kernel);
    Assembly Assembly { get; }
  }
}
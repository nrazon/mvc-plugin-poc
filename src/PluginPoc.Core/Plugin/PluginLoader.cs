﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition.Hosting;
using System.IO;
using System.Linq;
using System.Web.Hosting;
using System.Web.Mvc;
using System.Web.Routing;
using PluginPoc.Core.Modules;

namespace PluginPoc.Core.Plugin
{
  public class PluginLoader
  {
    public static IList<IPluginConfiguration> Plugins = new List<IPluginConfiguration>();

    public static void Load()
    {
      var plugins = LoadPlugins();

      foreach (var export in plugins)
      {
        var plugin = export.Value;
        plugin.RegisterRoutes(RouteTable.Routes);
        Plugins.Add(plugin);
        plugin.RegisterServices(ObjectFactory.Kernel);
        NinjectControllerFactory.RegisterControllers(plugin.Assembly);
      }
    }

    public static void Reload()
    {
      var plugins = LoadPlugins();

      var viewEngine = (PluginViewEngine)ViewEngines.Engines[0];

      foreach (var export in plugins)
      {
        var plugin = export.Value;

        var pluginConfiguration = Plugins.FirstOrDefault(x => x.Assembly == plugin.Assembly);
        if (pluginConfiguration == null)
        {
          plugin.RegisterRoutes(RouteTable.Routes);
          Plugins.Add(plugin);

          viewEngine.Add(plugin.Assembly);
          plugin.RegisterServices(ObjectFactory.Kernel);
          NinjectControllerFactory.RegisterControllers(plugin.Assembly);
        }
      }
    }

    private static IEnumerable<Lazy<IPluginConfiguration>> LoadPlugins()
    {
      var pluginsPath = HostingEnvironment.MapPath("~/plugins");

      if (pluginsPath == null)
        throw new DirectoryNotFoundException("plugins");

      var directoryCatalog = new DirectoryCatalog(pluginsPath);
      var container = new CompositionContainer(directoryCatalog);
      var exports = container.GetExports<IPluginConfiguration>();
      return exports;
    }
  }
}
﻿using System;
using Ninject;
using Ninject.Web.Common;

namespace PluginPoc.Core.Modules
{
  public class ObjectFactory
  {
    public static T GetInstance<T>(string name)
    {
      return Kernel.Get<T>(name);
    }

    public static object GetInstance(Type type)
    {
      return Kernel.Get(type);
    }

    public static IKernel Kernel
    {
      get
      {
        return new Bootstrapper().Kernel;
      }
    }
  }
}
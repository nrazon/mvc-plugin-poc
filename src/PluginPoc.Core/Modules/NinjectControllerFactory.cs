using System;
using System.Linq;
using System.Reflection;
using System.Web.Mvc;
using System.Web.Routing;

namespace PluginPoc.Core.Modules
{
  public class NinjectControllerFactory : DefaultControllerFactory
  {
    protected override IController GetControllerInstance(RequestContext requestContext, Type controllerType)
    {
      if (controllerType == null)
      {
        object value;
        if (requestContext.RouteData.Values.TryGetValue("controller", out value))
          return ObjectFactory.GetInstance<IController>(value.ToString().ToLowerInvariant());

        return null;
      }
      return (IController)ObjectFactory.GetInstance(controllerType);
    }

    public static void RegisterControllers(Assembly assembly)
    {
      foreach (Type type in assembly.GetExportedTypes().Where(IsController))
      {
        var controllerName = GetControllerName(type);
        ObjectFactory.Kernel.Bind<IController>().To(type).InHybridScope().Named(controllerName);
      }
    }

    private static bool IsController(Type type)
    {
      return typeof(IController).IsAssignableFrom(type) && type.IsPublic && !type.IsAbstract && !type.IsInterface;
    }

    private static string GetControllerName(Type type)
    {
      string name = type.Name.ToLowerInvariant();

      if (name.EndsWith("controller"))
        name = name.Substring(0, name.IndexOf("controller", StringComparison.Ordinal));

      return name;
    }
  }
}
﻿using Ninject.Activation;
using Ninject.Syntax;

namespace PluginPoc.Core.Modules
{
  public static class HybridScopeExtensionMethod
  {
    public static IBindingNamedWithOrOnSyntax<T> InHybridScope<T>(this IBindingInSyntax<T> syntax)
    {
      return syntax.InScope(HybridScope);
    }

    public static object HybridScope(IContext context)
    {
      object httpContext = System.Web.HttpContext.Current;
      object thread = System.Threading.Thread.CurrentThread;
      return httpContext ?? thread;
    }
  }
}
﻿using System.Web.Mvc;
using System.Web.Routing;
using Ninject;
using PluginPoc.Core.Modules;
using PluginPoc.Host.App_Start;
using PluginPoc.Host.Modules;

namespace PluginPoc.Host
{
  // Note: For instructions on enabling IIS6 or IIS7 classic mode, 
  // visit http://go.microsoft.com/?LinkId=9394801
  public class MvcApplication : System.Web.HttpApplication
  {
    protected void Application_Start()
    {
      AreaRegistration.RegisterAllAreas();

      FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
      RouteConfig.RegisterRoutes(RouteTable.Routes);

      ObjectFactory.Kernel.Load<ServicesNinjectModule>();
      ControllerBuilder.Current.SetControllerFactory(new NinjectControllerFactory());
    }
  }
}
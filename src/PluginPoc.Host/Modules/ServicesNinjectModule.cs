﻿using Ninject.Modules;
using PluginPoc.Core.Modules;
using PluginPoc.Core.Services;

namespace PluginPoc.Host.Modules
{
  public class ServicesNinjectModule : NinjectModule
  {
    public override void Load()
    {
      Bind<IFooService>()
        .To<FooService>()
        .InHybridScope();
    }
  }
}
﻿using System.Web.Mvc;
using PluginPoc.Core.Plugin;

namespace PluginPoc.Host.Controllers
{
  public class HomeController : Controller
  {
    public ActionResult Index()
    {
      return View();
    }

    public ActionResult AddPlugins()
    {
      PluginLoader.Reload();
      return RedirectToAction("Index");
    }
  }
}

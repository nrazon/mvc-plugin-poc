﻿using System.ComponentModel.Composition;
using System.Web.Mvc;
using System.Web.Routing;
using PluginPoc.Core.Plugin;

namespace PluginPoc.Plugin2
{
  [Export(typeof(IPluginConfiguration))]
  public class PluginConfiguration : AbstractPluginConfiguration
  {
    public override void RegisterRoutes(RouteCollection routes)
    {
      routes.MapRoute(
          name: "Plugin2",
          url: "CustomRoute2/{action}/{id}",
          defaults: new { controller = "Plugin2", action = "Index", id = UrlParameter.Optional }
      );
    }
  }
}